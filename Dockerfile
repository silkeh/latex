FROM silkeh/latex:pandoc

COPY . /usr/local/texlive/texmf-local/
RUN texhash

RUN apk add graphviz
