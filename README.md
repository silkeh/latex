# LaTeX TeXmf

## Install

Clone this directory directly in your `texmf` directory as follows:

    git clone --recursive https://git.slxh.eu/tex/latex-texmf.git $(kpsewhich --var-value TEXMFHOME)

Update the font locations afterwards using:

    updmap -user

## Update

Use the following commands to update this:

    cd $(kpsewhich --var-value TEXMFHOME)
    git pull
    git submodule update --init

## Misc

The included `symlinks.sh` script can be used to recreate the directory structure when another submodule is added in git.
